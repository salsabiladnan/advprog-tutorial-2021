## Lazy Instantiation 

Lazy instantiation adalah pembuatan dari sebuah object dilakukan hanya ketika `getInstance()` dipanggil.

**Keuntungan**:
- Penggunaan memori lebih hemat

**Kekurangan**:
- Sulit menangani isu multithreading

## Eager Instantiation 

Eager instantiation adalah pembuatan dari sebuah object dilakukan di awal dan `getInstance()` hanya mengembalikan object tersebut.

**Keuntungan**:
- Cocok untuk menangani isu multithreading

**Kekurangan**:
- Apabila tidak digunakan, object tetap dibuat sehingga akan memakan resource yang banyak