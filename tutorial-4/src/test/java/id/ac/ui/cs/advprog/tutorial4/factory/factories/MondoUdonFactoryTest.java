package id.ac.ui.cs.advprog.tutorial4.factory.factories;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factories.MondoUdonFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MondoUdonFactoryTest {
    private MondoUdonFactory mondoUdonFactory;

    @BeforeEach
    public void setup() throws Exception {
        mondoUdonFactory = new MondoUdonFactory();
    }

    @Test
    public void testMondoUdonFactoryCreateNoodle() throws Exception {
        Noodle noodle = mondoUdonFactory.createNoodle();
        assertEquals(noodle.getDescription(), "Adding Mondo Udon Noodles...");
    }

    @Test
    public void testMondoUdonFactoryCreateMeat() throws Exception {
        Meat meat = mondoUdonFactory.createMeat();
        assertEquals(meat.getDescription(), "Adding Wintervale Chicken Meat...");
    }

    @Test
    public void testMondoUdonFactoryCreateTopping() throws Exception {
        Topping topping = mondoUdonFactory.createTopping();
        assertEquals(topping.getDescription(), "Adding Shredded Cheese Topping...");
    }

    @Test
    public void testMondoUdonFactoryCreateFlavor() throws Exception {
        Flavor flavor = mondoUdonFactory.createFlavor();
        assertEquals(flavor.getDescription(), "Adding a pinch of salt...");
    }
}
