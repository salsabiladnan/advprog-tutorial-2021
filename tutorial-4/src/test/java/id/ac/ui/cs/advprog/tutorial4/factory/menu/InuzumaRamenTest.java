package id.ac.ui.cs.advprog.tutorial4.factory.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factories.InuzumaRamenFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class InuzumaRamenTest {
    private InuzumaRamen inuzumaRamen;

    @BeforeEach
    public void setup() {
        inuzumaRamen = new InuzumaRamen("Inuzuma Ramen");
    }

    @Test
    public void testInuzumaRamenReturnsRightName() {
        assertEquals(inuzumaRamen.getName(), "Inuzuma Ramen");
    }

    @Test
    public void testInuzumaRamenReturnsRightNoodle() {
        assertTrue(inuzumaRamen.getNoodle() instanceof Ramen);
    }

    @Test
    public void testInuzumaRamenReturnsRightMeat() {
        assertTrue(inuzumaRamen.getMeat() instanceof Pork);
    }

    @Test
    public void testInuzumaRamenReturnsRightTopping() {
        assertTrue(inuzumaRamen.getTopping() instanceof BoiledEgg);
    }

    @Test
    public void testInuzumaRamenReturnsRightFlavor() {
        assertTrue(inuzumaRamen.getFlavor() instanceof Spicy);
    }

    @Test
    public void testInuzumaRamenReturnsRightFactory() {
        assertTrue(inuzumaRamen.getFactory() instanceof InuzumaRamenFactory);
    }
}
