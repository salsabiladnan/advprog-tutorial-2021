package id.ac.ui.cs.advprog.tutorial4.singleton.Service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OrderServiceImplTest {
    private static OrderServiceImpl orderService;
    private static OrderFood orderFood;
    private static OrderDrink orderDrink;


    @BeforeEach
    public void setup()  {
        orderService = new OrderServiceImpl();
        orderFood = OrderFood.getInstance();
        orderDrink = OrderDrink.getInstance();
    }

    @Test
    public void testOrderServiceTestOrderFood() {
        String food = "Food";
        orderService.orderAFood(food);
        String result = orderService.getFood().getFood();
        assertEquals(food, result);
    }

    @Test
    public void testOrderServiceTestOrderDrink() {
        String drink = "Drink";
        orderService.orderADrink(drink);
        String result = orderService.getDrink().getDrink();
        assertEquals(drink, result);
    }

    @Test
    public void testOrderFoodOnlyOneInstance() {
        assertEquals(orderFood, orderService.getFood());
    }

    @Test
    public void testOrderDrinkOnlyOneInstance() {
        assertEquals(orderDrink, orderService.getDrink());
    }
}

