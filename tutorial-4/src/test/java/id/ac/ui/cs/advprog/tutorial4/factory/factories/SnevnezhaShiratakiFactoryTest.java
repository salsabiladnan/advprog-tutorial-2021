package id.ac.ui.cs.advprog.tutorial4.factory.factories;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factories.SnevnezhaShiratakiFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SnevnezhaShiratakiFactoryTest {
    private SnevnezhaShiratakiFactory snevnezhaShiratakiFactory;

    @BeforeEach
    public void setup() throws Exception {
        snevnezhaShiratakiFactory = new SnevnezhaShiratakiFactory();
    }

    @Test
    public void testSnevnezhaShiratakiFactoryCreateNoodle() throws Exception {
        Noodle noodle = snevnezhaShiratakiFactory.createNoodle();
        assertEquals(noodle.getDescription(), "Adding Snevnezha Shirataki Noodles...");
    }

    @Test
    public void testSnevnezhaShiratakiFactoryCreateMeat() throws Exception {
        Meat meat = snevnezhaShiratakiFactory.createMeat();
        assertEquals(meat.getDescription(), "Adding Zhangyun Salmon Fish Meat...");
    }

    @Test
    public void testSnevnezhaShiratakiFactoryCreateTopping() throws Exception {
        Topping topping = snevnezhaShiratakiFactory.createTopping();
        assertEquals(topping.getDescription(), "Adding Xinqin Flower Topping...");
    }

    @Test
    public void testSnevnezhaShiratakiFactoryCreateFlavor() throws Exception {
        Flavor flavor = snevnezhaShiratakiFactory.createFlavor();
        assertEquals(flavor.getDescription(), "Adding WanPlus Specialty MSG flavoring...");
    }
}
