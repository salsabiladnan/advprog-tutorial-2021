package id.ac.ui.cs.advprog.tutorial4.factory.factories;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factories.InuzumaRamenFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class InuzumaRamenFactoryTest {
    private InuzumaRamenFactory inuzumaRamenFactory;

    @BeforeEach
    public void setup() throws Exception {
        inuzumaRamenFactory = new InuzumaRamenFactory();

    }

    @Test
    public void testInuzumaRamenFactoryCreateNoodle() throws Exception {
        Noodle noodle = inuzumaRamenFactory.createNoodle();
        assertEquals(noodle.getDescription(), "Adding Inuzuma Ramen Noodles...");
    }

    @Test
    public void testInuzumaRamenFactoryCreateMeat() throws Exception {
        Meat meat = inuzumaRamenFactory.createMeat();
        assertEquals(meat.getDescription(), "Adding Tian Xu Pork Meat...");
    }

    @Test
    public void testInuzumaRamenFactoryCreateTopping() throws Exception {
        Topping topping = inuzumaRamenFactory.createTopping();
        assertEquals(topping.getDescription(), "Adding Guahuan Boiled Egg Topping");
    }

    @Test
    public void testInuzumaRamenFactoryCreateFlavor() throws Exception {
        Flavor flavor = inuzumaRamenFactory.createFlavor();
        assertEquals(flavor.getDescription(), "Adding Liyuan Chili Powder...");
    }
}
