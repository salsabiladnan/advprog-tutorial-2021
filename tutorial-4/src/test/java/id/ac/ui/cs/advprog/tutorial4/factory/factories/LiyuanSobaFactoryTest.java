package id.ac.ui.cs.advprog.tutorial4.factory.factories;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factories.InuzumaRamenFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.factories.LiyuanSobaFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LiyuanSobaFactoryTest {
    private LiyuanSobaFactory liyuanSobaFactory;

    @BeforeEach
    public void setup() throws Exception {
        liyuanSobaFactory = new LiyuanSobaFactory();
    }

    @Test
    public void testLiyuanSobaFactoryCreateNoodle() throws Exception {
        Noodle noodle = liyuanSobaFactory.createNoodle();
        assertEquals(noodle.getDescription(), "Adding Liyuan Soba Noodles...");
    }

    @Test
    public void testLiyuanSobaFactoryCreateMeat() throws Exception {
        Meat meat = liyuanSobaFactory.createMeat();
        assertEquals(meat.getDescription(), "Adding Maro Beef Meat...");
    }

    @Test
    public void testLiyuanSobaFactoryCreateTopping() throws Exception {
        Topping topping = liyuanSobaFactory.createTopping();
        assertEquals(topping.getDescription(), "Adding Shiitake Mushroom Topping...");
    }

    @Test
    public void testLiyuanSobaFactoryCreateFlavor() throws Exception {
        Flavor flavor = liyuanSobaFactory.createFlavor();
        assertEquals(flavor.getDescription(), "Adding a dash of Sweet Soy Sauce...");
    }
}
