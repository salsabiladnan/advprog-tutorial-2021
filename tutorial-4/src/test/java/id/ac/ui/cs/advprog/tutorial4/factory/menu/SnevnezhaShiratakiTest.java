package id.ac.ui.cs.advprog.tutorial4.factory.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factories.SnevnezhaShiratakiFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SnevnezhaShiratakiTest {
    private SnevnezhaShirataki snevnezhaShirataki;

    @BeforeEach
    public void setup() {
        snevnezhaShirataki = new SnevnezhaShirataki("Snevnezha Shirataki");
    }

    @Test
    public void testSnevnezhaShiratakiReturnsRightName() {
        assertEquals(snevnezhaShirataki.getName(), "Snevnezha Shirataki");
    }

    @Test
    public void testSnevnezhaShiratakiReturnsRightNoodle() {
        assertTrue(snevnezhaShirataki.getNoodle() instanceof Shirataki);
    }

    @Test
    public void testSnevnezhaShiratakiReturnsRightMeat() {
        assertTrue(snevnezhaShirataki.getMeat() instanceof Fish);
    }

    @Test
    public void testSnevnezhaShiratakiReturnsRightTopping() {
        assertTrue(snevnezhaShirataki.getTopping() instanceof Flower);
    }

    @Test
    public void testSnevnezhaShiratakiReturnsRightFlavor() {
        assertTrue(snevnezhaShirataki.getFlavor() instanceof Umami);
    }

    @Test
    public void testSnevnezhaShiratakiReturnsRightFactory() {
        assertTrue(snevnezhaShirataki.getFactory() instanceof SnevnezhaShiratakiFactory);
    }
}
