package id.ac.ui.cs.advprog.tutorial4.factory.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factories.LiyuanSobaFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LiyuanSobaTest {
    private LiyuanSoba liyuanSoba;

    @BeforeEach
    public void setup() {
        liyuanSoba = new LiyuanSoba("Liyuan Soba");
    }

    @Test
    public void testLiyuanSobaReturnsRightName() {
        assertEquals(liyuanSoba.getName(), "Liyuan Soba");
    }

    @Test
    public void testLiyuanSobaReturnsRightNoodle() {
        assertTrue(liyuanSoba.getNoodle() instanceof Soba);
    }

    @Test
    public void testLiyuanSobaReturnsRightMeat() {
        assertTrue(liyuanSoba.getMeat() instanceof Beef);
    }

    @Test
    public void testLiyuanSobaReturnsRightTopping() {
        assertTrue(liyuanSoba.getTopping() instanceof Mushroom);
    }

    @Test
    public void testLiyuanSobaReturnsRightFlavor() {
        assertTrue(liyuanSoba.getFlavor() instanceof Sweet);
    }

    @Test
    public void testLiyuanSobaReturnsRightFactory() {
        assertTrue(liyuanSoba.getFactory() instanceof LiyuanSobaFactory);
    }
}
