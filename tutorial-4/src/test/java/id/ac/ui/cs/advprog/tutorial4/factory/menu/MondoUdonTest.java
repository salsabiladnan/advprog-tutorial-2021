package id.ac.ui.cs.advprog.tutorial4.factory.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factories.MondoUdonFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MondoUdonTest {
    private MondoUdon mondoUdon;

    @BeforeEach
    public void setup() {
        mondoUdon = new MondoUdon("Mondo Udon");
    }

    @Test
    public void testMondoUdonReturnsRightName() {
        assertEquals(mondoUdon.getName(), "Mondo Udon");
    }

    @Test
    public void testMondoUdonReturnsRightNoodle() {
        assertTrue(mondoUdon.getNoodle() instanceof Udon);
    }

    @Test
    public void testMondoUdonReturnsRightMeat() {
        assertTrue(mondoUdon.getMeat() instanceof Chicken);
    }

    @Test
    public void testMondoUdonReturnsRightTopping() {
        assertTrue(mondoUdon.getTopping() instanceof Cheese);
    }

    @Test
    public void testMondoUdonReturnsRightFlavor() {
        assertTrue(mondoUdon.getFlavor() instanceof Salty);
    }

    @Test
    public void testMondoUdonReturnsRightFactory() {
        assertTrue(mondoUdon.getFactory() instanceof MondoUdonFactory);
    }
}
