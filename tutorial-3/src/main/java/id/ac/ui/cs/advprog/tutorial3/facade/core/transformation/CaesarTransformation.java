package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;

public class CaesarTransformation {
    private int key;

    public CaesarTransformation(int key){
        this.key = key;
    }

    public CaesarTransformation(){
        this.key = 5;
    }

    public Spell encode(Spell spell){
        return process(spell, true);
    }

    public Spell decode(Spell spell){
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode){
        String text = spell.getText();
        Codex codex = spell.getCodex();

        int selector = encode ? -1 : 1;
        int alphaCodexSize = codex.getCharSize();

        int n = text.length();

        char[] res = new char[n];
        for(int i = 0; i < n; i++){
            char oldChar = text.charAt(i);
            int newIdx = codex.getIndex(oldChar) + (key*selector);
            if (newIdx < 0) {
                newIdx = alphaCodexSize + newIdx;
            } else {
                newIdx = newIdx % alphaCodexSize;
            }

            res[i] = codex.getChar(newIdx);
        }

        return new Spell(new String(res), codex);



    }

}
