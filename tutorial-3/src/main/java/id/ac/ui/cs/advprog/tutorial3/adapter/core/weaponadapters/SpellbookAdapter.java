package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private int count;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.count = 0;
    }

    @Override
    public String normalAttack() {
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if (count < 1) {
            count += 1;
            return spellbook.largeSpell();
        } else {
            count = 0;
            return "Can't cast large spell twice in a row";
        }

    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
