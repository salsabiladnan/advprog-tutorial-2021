package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class FacadeTransformation {
    private AbyssalTransformation abyssalTransformation;
    private CelestialTransformation celestialTransformation;
    private CaesarTransformation caesarTransformation;

    public FacadeTransformation() {
        this.abyssalTransformation = new AbyssalTransformation();
        this.celestialTransformation = new CelestialTransformation();
        this.caesarTransformation = new CaesarTransformation();
    }

    public String encode(String text){
        AlphaCodex alphaCodex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, alphaCodex);
        spell = celestialTransformation.encode(spell);
        spell = abyssalTransformation.encode(spell);
        spell = caesarTransformation.encode(spell);//
        RunicCodex runicCodex = RunicCodex.getInstance();
        spell = CodexTranslator.translate(spell, runicCodex);

        return spell.getText();
    }

    public String decode(String code){
        RunicCodex runicCodex = RunicCodex.getInstance();
        Spell spell = new Spell(code, runicCodex);
        AlphaCodex alphaCodex = AlphaCodex.getInstance();
        spell = CodexTranslator.translate(spell, alphaCodex);
        spell = caesarTransformation.decode(spell);
        spell = abyssalTransformation.decode(spell);
        spell = celestialTransformation.decode(spell);

        return spell.getText();
    }

}

