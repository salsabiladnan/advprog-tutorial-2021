package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private WeaponRepository weaponRepository;

    @Autowired
    private BowRepository bowRepository;

    @Autowired
    private SpellbookRepository spellbookRepository;

    @Override
    public List<Weapon> findAll() {
        List<Spellbook> listOfSpellbooks = spellbookRepository.findAll();
        List<Bow> listOfBows = bowRepository.findAll();

        for (Spellbook spell : listOfSpellbooks) {
            SpellbookAdapter spellAdapter = new SpellbookAdapter(spell);
            if (weaponRepository.findByAlias(spell.getName()) == null) {
                weaponRepository.save(spellAdapter);
            }
        }

        for (Bow bow : listOfBows) {
            BowAdapter bowAdapter = new BowAdapter(bow);
            if (weaponRepository.findByAlias(bow.getName()) == null) {
                weaponRepository.save(bowAdapter);
            }
        }
        return weaponRepository.findAll();
    }

    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        String type = "";
        String attackDesc = "";
        Weapon newWeapon = weaponRepository.findByAlias(weaponName);

        if (attackType ==  1) {
            type = " (normal attack): ";
            attackDesc = newWeapon.normalAttack();
        } else if (attackType == 2) {
            type = " (charged attack): ";
            attackDesc = newWeapon.chargedAttack();
        }

        logRepository.addLog(newWeapon.getHolderName() + " attacked with " + weaponName + type + attackDesc);
        weaponRepository.save(newWeapon);

    }

    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
